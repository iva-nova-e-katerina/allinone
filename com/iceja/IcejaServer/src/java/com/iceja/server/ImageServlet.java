/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iceja.server;

import com.inet.html.InetHtmlConfiguration;
import com.inet.html.InetHtmlEditorKit;
import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import javax.swing.plaf.basic.BasicTextPaneUI;
import javax.swing.text.EditorKit;
import javax.swing.text.JTextComponent;
import org.lobobrowser.html.HtmlRendererContext;
import org.lobobrowser.html.UserAgentContext;
import org.lobobrowser.html.gui.HtmlPanel;
import org.lobobrowser.html.parser.DocumentBuilderImpl;
import org.lobobrowser.html.parser.InputSourceImpl;
import org.lobobrowser.html.test.SimpleHtmlRendererContext;
import org.lobobrowser.html.test.SimpleUserAgentContext;
import org.w3c.dom.Document;
import org.w3c.dom.html2.HTMLElement;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author kitty
 */
public class ImageServlet extends HttpServlet {

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("image/png");
        String root = request.getParameter("url");
        URL newUrl = null;
        try {
            newUrl = new URL(root);
        } catch (MalformedURLException ex) {
            // mailformed URL
            ex.printStackTrace();
            return;
        }
        BufferedImage rendered = null;

       // try {
            String content = URLServlet.convertStreamToString(newUrl.openStream());
            //rendered = renderImage(content, newUrl.toURI());
            rendered = renderImage2(content, newUrl);
//        } catch (URISyntaxException ex) {
//            ex.printStackTrace();
//            return;
//        }

        //ImageInputStream iis = ImageIO.createImageInputStream(rendered);


        BufferedOutputStream out = new BufferedOutputStream(response.getOutputStream());
//        String filename = request.getSession().getServletContext().getRealPath("/") + "/" + request.getParameter("tag") + ".jpg";
//        File image = new File(filename);
//        FileInputStream fis = new FileInputStream(image);

//          int[] buffer = ((DataBufferInt)(rendered).getRaster().getDataBuffer()).getData();
//          ByteArrayInputStream bais = new ByteArrayInputStream(buffer);
//      //    IntArrayInputStream
//        //  BufferedInputStream in = new BufferedInputStream(buffer);
////BufferedInputStream;
////        ByteArrayInputStream; DataInputStream
//
//        try {
//
//            byte b[] = new byte[8];
//            int count;
//            while ((count = bais.read(b)) != -1) {
//                out.write(b, 0, count);
//            }
//        } finally {
//            out.flush();
//            out.close();
//            bais.close();
//        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    private BufferedImage renderImage2(String content, URL url) throws IOException {
         final InetHtmlEditorKit kit = new InetHtmlEditorKit();
        kit.setDefaultConfig( InetHtmlConfiguration.getBrowserConfig() );
        JFrame someWindow = new JFrame();
        JEditorPane webEngine = new JEditorPane();
        webEngine.setOpaque( false );
        webEngine.setEditorKit( kit );
         Listener l = new Listener();
        webEngine.addHyperlinkListener( l );
        webEngine.setUI( new BasicTextPaneUI(){
            @Override
            public EditorKit getEditorKit( JTextComponent tc ) {
                return kit;
            }
        });
        webEngine.setEditable( false );
        
        someWindow.setBounds(30, 30, 750, 750);
        someWindow.add(new JScrollPane(webEngine));
        webEngine.setPage( url );
        someWindow.setVisible(true);

//                JScrollPane webScroll = new JScrollPane( webEngine );
//        webScroll.setDoubleBuffered( true );
//        JPanel webEnginePanel = new JPanel( new BorderLayout() );
//        webEnginePanel.add( webScroll, BorderLayout.CENTER );
//        webEnginePanel.add( new JLabel( "JWebEngine Renderer" ), BorderLayout.NORTH );
//        webEngine.setPage( url );
        
        
                BufferedImage bi = new BufferedImage(300,250,BufferedImage.TYPE_INT_RGB);
        webEngine.paint(bi.getGraphics());
        ImageIO.write(bi, "png", new File(Thread.currentThread().getName()));
            System.out.println("file saved with name " + Thread.currentThread().getName() + "png" );
        
        return null;
    }

    private BufferedImage renderImage(String content, URI uri) {
        try {
            // Initialize logging so only Cobra warnings are seen.
            // Open a connection on the URL we want to render first.
            //		String uri = "http://lobobrowser.org/browser/home.jsp";
            //		URL url = new URL(uri);
            //		URLConnection connection = url.openConnection();
            //		InputStream in = connection.getInputStream();
            // A Reader should be created with the correct charset,
            // which may be obtained from the Content-Type header
            // of an HTTP response.
            Reader reader = new StringReader(content);
            //Reader reader = new InputStreamReader(in);
            // InputSourceImpl constructor with URI recommended
            // so the renderer can resolve page component URLs.
            InputSource is = new InputSourceImpl(reader, uri.toString());
            final GrabHtml htmlPanel = new GrabHtml();
            //    new SimpleHtmlRendererContext(panel, new SimpleUserAgentContext()).navigate("http://lobobrowser.org/browser/home.jsp");
            //   UserAgentContext ucontext = new LocalUserAgentContext();
            //   HtmlRendererContext rendererContext = new LocalHtmlRendererContext(htmlPanel, ucontext);

            UserAgentContext ucontext = new SimpleUserAgentContext();
            HtmlRendererContext rendererContext = new LocalHtmlRendererContext(htmlPanel, ucontext);
            // Set a preferred width for the HtmlPanel,
            // which will allow getPreferredSize() to
            // be calculated according to block content.
            // We do this here to illustrate the
            // feature, but is generally not
            // recommended for performance reasons.
            htmlPanel.setPreferredWidth(1024);
            // Note: This example does not perform incremental
            // rendering while loading the initial document.
            DocumentBuilderImpl builder = new DocumentBuilderImpl(rendererContext.getUserAgentContext(), rendererContext);
            Document document = builder.parse(is);
            reader.close();
            // Set the document in the HtmlPanel. This method
            // schedules the document to be rendered in the
            // GUI thread.
            htmlPanel.setDocument(document, rendererContext);
//            BufferedImage hello = new BufferedImage(htmlPanel.getWidth(), htmlPanel.getHeight(), BufferedImage.TYPE_INT_RGB);
            // htmlPanel.paint(htmlPanel.getGraphics());

            // Create a JFrame and add the HtmlPanel to it.
            final JFrame frame = new JFrame();
            frame.getContentPane().add(htmlPanel);
//            frame.pack();
//            frame.setVisible(true);

            System.out.println("width  " + htmlPanel.getWidth() + " height " + htmlPanel.getHeight() + "  " + htmlPanel.getBounds());
            //  System.exit(0);
//
            // We pack the JFrame to demonstrate the
            // validity of HtmlPanel's preferred size.
            // Normally you would want to set a specific
            // JFrame size instead.
            // pack() should be called in the GUI dispatch
            // thread since the document is scheduled to
            // be rendered in that thread, and is required
            // for the preferred size determination.

            ImageHolder render = new ImageHolder(frame, htmlPanel, this);
            System.out.println("Invoker srarts !!!!!!!!!!!!!!!!!!!!!!");
            EventQueue.invokeLater(render);





//            try {
//                        ImageIO.write(htmlPanel.getImage(), "png", new File(Thread.currentThread().getName()));
//                    } catch (IOException ex) {
//                        Logger.getLogger(UrlGrapgHolder.class.getName()).log(Level.SEVERE, null, ex);
//                    }
//               final JFrame frame = new JFrame();
//       frame.getContentPane().add(htmlPanel);
//            frame.pack();
//            BufferedImage hello = new BufferedImage(htmlPanel.getWidth(), htmlPanel.getHeight(), BufferedImage.TYPE_INT_RGB);
//            Graphics g = hello.getGraphics();
//            htmlPanel.paint(g);
//            ImageIO.write(hello, "png", new File(Thread.currentThread().getName()));
            System.out.println("Invoker before join !!!!!!!!!!!!!!!!!!!!!!");
            //render.join();
            synchronized (this) {
                this.wait();
            }

            System.out.println("Invoker ends !!!!!!!!!!!!!!!!!!!!!!");
            return render.getImg();
        } catch (InterruptedException ex) {

            ex.printStackTrace();
        } catch (SAXException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private static class LocalHtmlRendererContext
            extends SimpleHtmlRendererContext {
        // Override methods from SimpleHtmlRendererContext
        // to provide browser functionality to the renderer.

        public LocalHtmlRendererContext(HtmlPanel contextComponent,
                UserAgentContext ucontext) {
            super(contextComponent, ucontext);
        }

        public void linkClicked(HTMLElement linkNode,
                URL url, String target) {
            super.linkClicked(linkNode, url, target);
            // This may be removed:
            System.out.println("## Link clicked: " + linkNode);
        }

        public HtmlRendererContext open(URL url,
                String windowName, String windowFeatures,
                boolean replace) {
            // This is called on window.open().
            HtmlPanel newPanel = new HtmlPanel();
            JFrame frame = new JFrame();
            frame.setSize(600, 400);
            frame.getContentPane().add(newPanel);
            HtmlRendererContext newCtx = new LocalHtmlRendererContext(newPanel, this.getUserAgentContext());
            newCtx.navigate(url, "_this");
            return newCtx;
        }
    }
    
    private class Listener implements HyperlinkListener {

        /**
         * {@inheritDoc}
         */
        public void hyperlinkUpdate( HyperlinkEvent e ) {
            if( e.getEventType() == HyperlinkEvent.EventType.ACTIVATED ){
                //setPages( e.getURL() );
            }
        }
        
    }
}
