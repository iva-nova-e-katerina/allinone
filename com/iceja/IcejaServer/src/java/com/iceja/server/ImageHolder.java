/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iceja.server;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.JFrame;

/**
 *
 * @author Catherine Ivanova
 * @date Feb 6, 2011
 */
public class ImageHolder implements Runnable {
    
    private JFrame frame;
    private GrabHtml htmlPanel;
    private ImageServlet servlet;

    private BufferedImage img;

    public ImageHolder (JFrame frame , GrabHtml htmlPanel,  ImageServlet servlet){
        this.frame = frame;
        this.htmlPanel = htmlPanel;
        this.servlet = servlet;
    }

    @Override
    public void run() {
           finalStageRender(frame, htmlPanel);
    }

    private void finalStageRender(JFrame frame, GrabHtml htmlPanel) {
        System.out.println("RENDER STARTS !!!!!!!!!!!!!!!!!!!!");
        frame.pack();
        // frame.setVisible(true);
        int width = htmlPanel.getWidth() > 2048 ? 2048 : htmlPanel.getWidth();
        int height = htmlPanel.getHeight() > 1024 ? 1024 : htmlPanel.getHeight();
        System.out.println("htmlPanel.getWidth()  " + htmlPanel.getWidth() + " htmlPanel.getHeight() " + htmlPanel.getHeight() + "  " + htmlPanel.getBounds());
        if (width > 0 && height > 0) {
            System.out.println("width  " + width + " height " + height + "  " + htmlPanel.getBounds());
            //                        BufferedImage hello = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
            //                        Graphics g = hello.getGraphics();
            BufferedImage hello = htmlPanel.getImage();
            this.img =  hello;
      //      hello.
            try {
                File img = new File(Thread.currentThread().getName() + System.currentTimeMillis() + ".png");
                ImageIO.write(hello, "png", img);
                System.out.println("saved " + img.getAbsolutePath());

                synchronized(servlet){
                    servlet.notifyAll();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        System.out.println("RENDER ENDS !!!!!!!!!!!!!!!!!!!!");
    }

    /**
     * @return the img
     */
    public BufferedImage getImg() {
        return img;
    }

    /**
     * @param img the img to set
     */
    public void setImg(BufferedImage img) {
        this.img = img;
    }


}
