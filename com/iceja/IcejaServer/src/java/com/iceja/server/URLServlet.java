/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iceja.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.net.MalformedURLException;
import java.net.URL;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author kitty
 */
public class URLServlet extends HttpServlet {

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String root = request.getParameter("url");
        System.out.println("0");
        System.out.println(request.getParameterMap());
        PrintWriter out = response.getWriter();
        try {
            String [] urls = getURLS(root, true);
            for(String s: urls){
                out.println(s);
            }

            out.println("test ");
            System.out.println("1");
            return;
        } catch (Exception e){
            e.printStackTrace();
        } finally {
            out.close();
            System.out.println("2");
        }
    }

    private String[] getURLS(String root, boolean urls) throws IOException {
        String resultUrls = new String();
        URL targetUrl = null;
        System.out.println("root " + root);
        targetUrl = new URL(root);

        String content = convertStreamToString(targetUrl.openStream());
        //Image face = renderContent(content);

        String[] scanner = content.split("\\s*href=\\s*");
        for (String preUrl : scanner) {
            int begin = 0;
            char quotes = '\'';
            if (preUrl.startsWith("\"")) {
                quotes = '"';
                begin++;
            } else if (preUrl.startsWith("'")) {
                begin++;
            } else {
                quotes = ' ';

            }
            quotes = preUrl.indexOf(quotes, begin) <= -1 ? '>' : quotes;
            int end = preUrl.indexOf(quotes, begin);
            if (end <= -1 || end <= begin) {
                continue;
            }
            String urlString = preUrl.substring(begin, end);
            
            URL newUrl = null;
            try {
                newUrl = new URL(urlString);
            } catch (MalformedURLException ex) {
                // mailformed URL
                continue;
            }
            resultUrls = urlString.concat(" ");


        }
        return resultUrls.split(" ");

    }

    public static String convertStreamToString(InputStream is) throws IOException {
        if (is != null) {
            Writer writer = new StringWriter();
            char[] buffer = new char[1024];
            Reader reader = new BufferedReader(new InputStreamReader(is));
            int n;
            while ((n = reader.read(buffer)) != -1) {
                writer.write(buffer, 0, n);
            }
            writer.flush();
            return writer.toString();

        }

        return "";
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
