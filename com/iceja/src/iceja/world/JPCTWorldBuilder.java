/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package iceja.world;

import com.threed.jpct.Object3D;
import com.threed.jpct.Primitives;
import com.threed.jpct.SimpleVector;
import com.threed.jpct.Texture;
import com.threed.jpct.World;
import iceja.HostURL;
import iceja.JpctTest2;
import iceja.UrlGrapgHolder;
import iceja.utils.ImageUtils;
import java.awt.image.BufferedImage;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jgrapht.graph.DefaultEdge;

/**
 *
 * @author Catherine Ivanova
 * Jan 6, 2011
 */
public class JPCTWorldBuilder extends AbstractWorldBuilder {

    private int depth;
    private World world = null;
    public ConcurrentHashMap<Object3D, URL> blocks = new ConcurrentHashMap<Object3D, URL>();
    private UrlGrapgHolder holder;
    int sleepTime;
    URL rootNode;

    public JPCTWorldBuilder(World world, UrlGrapgHolder holder, URL rootNode, int sleepTime) {
        this.world = world;
        this.holder = holder;
        this.sleepTime = sleepTime;
        this.rootNode = rootNode;
    }

    @Override
    public void run() {
        try {
            while (true) {
                buildWorld(this.rootNode);
                Thread.sleep(this.sleepTime);
                this.rootNode = selectRootNode();
                Thread.sleep(this.sleepTime);
            }
        } catch (InterruptedException ex) {
            Logger.getLogger(JPCTWorldBuilder.class.getName()).log(Level.SEVERE, null, ex);
        } catch (MalformedURLException ex) {
            Logger.getLogger(JPCTWorldBuilder.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void addChildren(URL root) {
        if (depth > 8) {
            return;
        }
        System.out.println("3 there are  " + holder.getUrls().size() + " blocks in holder , depth " + depth);
        LinkedHashSet<URL> ursl = (LinkedHashSet<URL>) getNeibourgs(root);
        System.out.println(root.getHost() + " has  " + ursl.size());
//        for(int i = 0 ; i < ursl.size(); i++){
//            URL url = ursl.
//
        int i = 0;
        for (URL url : ursl) {
            System.out.println("from " + root.getHost() + " to " + url.getHost());
            i++;
            if(!blocks.containsValue(url))
                createBlock(url, ursl.size(), i);

            depth++;
            addChildren(url);
            depth--;
        }

    }

    private void createBlock(URL url, int sizeOfPeice, int numInpeice) {
        Object3D box = Primitives.getBox(13f, 2f);
        box.setSelectable(Object3D.MOUSE_SELECTABLE);
        box.setCollisionMode(Object3D.COLLISION_CHECK_OTHERS);
        box.build();

        world.addObject(box);
        attachTexture(box, url);
        box.translate(new SimpleVector(numInpeice * 25 - ((sizeOfPeice * 25) / 2), -14 * depth - 420, 25 * depth));
        // world.getCamera().lookAt(box.getTransformedCenter());
        System.out.println("box added " + box.toString() + "  z=" + 14 * depth + 14);
        blocks.put(box, url);

    }

    private void attachTexture(Object3D object, URL url) {
        System.out.println(" 7 there are  " + holder.getUrls().size() + " blocks in holder and face of current block has ");
        for (HostURL hurl : holder.getUrls()) {
            System.out.println(" tested " + hurl.toString() + " looked " + url.getHost().toString());
            if (hurl.getUrl().equals(url)) {
                BufferedImage texture = (BufferedImage) hurl.getFace();

                //texture = (BufferedImage) ImageUtils.crop1(texture, 0, 0, texture.getWidth(), 256);
                texture = (BufferedImage) ImageUtils.scale1(texture, 256, (int) ((float) texture.getWidth() / (float) 256 * texture.getHeight()));
                texture = (BufferedImage) ImageUtils.crop1(texture, 0, 0, texture.getWidth(), 256);
                JpctTest2.tm.addTexture(url.toString(), new Texture(texture));
                object.setTexture(url.toString());
                System.out.println(" 6 there are  " + holder.getUrls().size() + " blocks in holder and face of current block has " + texture.getHeight() +  "  " + texture.getWidth() );
                
                return;
            }
        }

    }

    private Set<URL> getNeibourgs(URL node) {
        LinkedHashSet<URL> neighbors = new LinkedHashSet<URL>();
        System.out.println("4 there are  " + holder.getUrls().size() + " blocks in holder , depth " + depth);
        synchronized (UrlGrapgHolder.class) {
            if (holder.getHrefGraph().containsVertex(node)) {
                Set<DefaultEdge> egdes = holder.getHrefGraph().outgoingEdgesOf(node);

                for (DefaultEdge de : egdes) {
//            System.out.println("holder.getHrefGraph().vertexSet().contains(node) " + holder.getHrefGraph().vertexSet().contains(node));
//            if (!holder.getHrefGraph().vertexSet().contains(node)) {
                    //  System.out.println(" egde of " + node.getHost() + " is " + de.toString());
                    neighbors.add(holder.getHrefGraph().getEdgeTarget(de));
//            }
                }
            }
        }
        System.out.println(" 5 there are  " + holder.getUrls().size() + " blocks in holder");
        return neighbors;
    }

    private void buildWorld(URL fromURL) throws MalformedURLException {
        System.out.println("2 there are  " + holder.getUrls().size() + " blocks in holder , depth " + depth);
        addChildren(fromURL);
    }

    private URL selectRootNode() {
        return this.rootNode;
    }
}
