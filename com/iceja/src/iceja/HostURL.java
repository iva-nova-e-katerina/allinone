/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package iceja;

import java.awt.Image;
import java.net.URL;

/**
 *
 * @author Catherine Ivanova
 * Jan 4, 2011
 */
public class HostURL implements Comparable {
    private Image face;
    private URL url;
    private int depthFromRoot;

    public HostURL(URL url, int depth) {
        this.url = url;
        this.depthFromRoot = depth;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || !obj.getClass().equals(HostURL.class)) {
            return false;
        }
        HostURL objurl = (HostURL) obj;
        if (objurl.getUrl() == null || !objurl.url.getHost().equals(this.url.getHost())) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    public String toString() {

        return getUrl().getHost();

    }

    /**
     * @return the face
     */
    public Image getFace() {
        return face;
    }

    /**
     * @param face the face to set
     */
    public void setFace(Image face) {
        this.face = face;
    }

    /**
     * @return the depthFromRoot
     */
    public int getDepthFromRoot() {
        return depthFromRoot;
    }

    /**
     * @param depthFromRoot the depthFromRoot to set
     */
    public void setDepthFromRoot(int depthFromRoot) {
        this.depthFromRoot = depthFromRoot;
    }

    /**
     * @return the url
     */
    public URL getUrl() {
        return url;
    }

    /**
     * @param url the url to set
     */
    public void setUrl(URL url) {
        this.url = url;
    }

    public int compareTo(Object o) {
        if(this.equals(o)){
            return 0;
        } else {
            return -1;
        }
    }
}
