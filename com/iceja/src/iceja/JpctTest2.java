package iceja;


import com.threed.jpct.Config;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import javax.swing.JApplet;

import com.threed.jpct.FrameBuffer;
import com.threed.jpct.Lights;
import com.threed.jpct.Object3D;
import com.threed.jpct.Primitives;
import com.threed.jpct.SimpleVector;
import com.threed.jpct.Texture;
import com.threed.jpct.TextureManager;
import com.threed.jpct.World;
import iceja.world.JPCTWorldBuilder;
import iceja.controls.JPCTControl;
import java.awt.Frame;
import java.awt.Panel;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import netscape.javascript.JSObject;
import theme.JPCTThemeManager;
import theme.ThemeManager;

public class JpctTest2 extends JApplet implements Runnable {

    private FrameBuffer buffer = null;
    private World world = null;
    private boolean alive = true;
    private boolean initialized = false;
    UrlGrapgHolder holder;
    public static TextureManager tm = TextureManager.getInstance();
    private Object3D terrain = null;
    JPCTWorldBuilder wb;
    long iters = 1;
    View panel;
    int oldX;
    int oldY;
    Texture newYork;
    JSObject window;
    private final int yAngleLimit = 80;
    Object3D plane;
    Frame frame ;
    ThemeManager themeManager = new JPCTThemeManager();


    @Override
    // Initialize all components of the applet
    public void init() {
        try {
            holder = new UrlGrapgHolder(new HostURL(new URL("http://www.yahoo.com"), 0));

        } catch (MalformedURLException ex) {
            Logger.getLogger(JpctTest2.class.getName()).log(Level.SEVERE, null, ex);
        }
        Texture rocks = new Texture(Main.getTexture("/resources/rocks.jpg"));
        Texture test = new Texture(Main.getTexture("/resources/test.jpg"));
        tm.addTexture("rocks", rocks);
        tm.addTexture("test", test);

        Thread holderStarter = new Thread(holder);
        holderStarter.setPriority(Thread.MIN_PRIORITY);
       // holderStarter.start();
        
        world = new World();
        buffer = new FrameBuffer(getWidth(), getHeight(),
                FrameBuffer.SAMPLINGMODE_NORMAL);
        window = JSObject.getWindow(this);

        plane = Primitives.getPlane(2, 21000);
        plane.rotateX(3.14f / 2f);
        plane.setSpecularLighting(true);
        plane.setTexture("rocks");
        plane.setCollisionMode(Object3D.COLLISION_CHECK_OTHERS);

        world.addObject(plane);

        // world = new World();  // create a new world


        // Add some light to the scene
        world.setAmbientLight(0, 255, 0);
        /**
         * Setup the lighting. We are not using overbright lighting because the OpenGL
         * renderer can't do it, but we are using RGB-scaling. Some hardware/drivers
         * for OpenGL don't support this (but most do).
         */
        Config.fadeoutLight = false;
        world.getLights().setOverbrightLighting(Lights.OVERBRIGHT_LIGHTING_DISABLED);
        world.getLights().setRGBScale(Lights.RGB_SCALE_2X);
        world.setAmbientLight(25, 30, 30);

        /**
         * Place the lightsources...
         */
        world.addLight(new SimpleVector(0, -150, 0), 25, 22, 19);
        world.addLight(new SimpleVector(-1000, -150, 1000), 22, 5, 4);
        world.addLight(new SimpleVector(1000, -150, -1000), 4, 2, 22);


        // create a new buffer to draw on:

        World.setDefaultThread(Thread.currentThread());
     //   newYork = new Texture(themeManager.getISfromImage(themeManager.getForegraundTexture("/resources/newyork_big.jpg", 1280, 666)));
        //newYork = new Texture(Main.getTexture("/resources/newyork.jpg"));
        //System.out.println("h " + newYork.getHeight() + " w " + newYork.getWidth());
        //tm.addTexture("newyork", newYork);

        // set the camera's position:
        world.getCamera().setPosition(50, -650, -5);
        world.getCamera().setFOV(world.getCamera().getMaxFOV());
        try {

            //buildWorld(new URL("http://www.yahoo.com"));
            wb = new JPCTWorldBuilder(world, holder, new URL("http://www.yahoo.com"), 1000);
            //wb.start();
        } catch (MalformedURLException ex) {
            Logger.getLogger(JpctTest2.class.getName()).log(Level.SEVERE, null, ex);
        }

        panel = new View();

        panel.control = new JPCTControl(0, 0, window, world, wb, buffer, plane, this);
        addMouseListener(panel.control);
        addMouseMotionListener(panel.control);
        addMouseWheelListener(panel.control);
        addKeyListener(panel.control);

        add("Center", panel);
        //  addMouseListener(panel.control);

//        //1. Create the frame.
//        frame = new Frame("FrameDemo");
//
//        frame.setSize( 300, 300 );
//        frame.setVisible(true);
////        add(frame);
////        frame.show();

        // Finished initializing.  Now it is safe to paint.
        //Config.isIndoor = true;
        Config.tuneForOutdoor();
        initialized = true;
        Timer switcher = new Timer();
        switcher.scheduleAtFixedRate(new TimerTask() {

            @Override
            public void run() {

                iters *= -1;
            }
        }, 3000, 3000);



        world.setClippingPlanes(Config.nearPlane, 21000);
        // Start the main "Game Loop":
        terrain = Primitives.getBox(210f, 0.01f);
        //terrain.calcTextureWrapSpherical();//
        terrain.calcTextureWrap();
        //terrain.setTexture("rocks");
        terrain.setTexture("test");
        // terrain.setBaseTexture("rocks");
        terrain.build();

        world.addObject(terrain);
        terrain.translate(new SimpleVector(53 * 3, 2, 14 * 1 + 14));
        // world.getCamera().lookAt(terrain.getTransformedCenter());

        new Thread(this).start();
        // public void play(URL url, String name);

    }

    // Main Game Loop:
    @Override
    public void run() {
        while (alive) {

            for (Object3D box : wb.blocks.keySet()) {

                box.rotateY(-0.0004f * iters);
                box.rotateX(-0.0004f * iters);


            }
            // Tell the applet to repaint itself:
            this.panel.repaint();
//            panel.control.poll();
//            panel.control.doMovement();
            // Sleep for a bit:
            try {
                Thread.sleep(2);
            } catch (Exception e) {
            }
//            try {
//                buildWorld();
//            } catch (MalformedURLException ex) {
//                Logger.getLogger(JpctTest2.class.getName()).log(Level.SEVERE, null, ex);
//            }
        }
    }

    // End the main game loop:
    @Override
    public void destroy() {
        alive = false;
    }

    class View extends Panel
            implements Runnable {

        private JPCTControl control;

        View() {
            control = new JPCTControl(0,0, window, world,wb,  buffer, plane, this);
            addMouseListener(control);
            addMouseMotionListener(control);
            addMouseWheelListener(control);
            addKeyListener(control);
            
        }

        public synchronized void update(Graphics g) {
            if (!initialized) {
                return;
            }

            buffer.clear();   // erase the previous frame

            // render the world onto the buffer:
            buffer.setBlittingTarget(FrameBuffer.BLITTING_TARGET_BACK);

            //System.out.println(buffer.getOutputWidth() + "  " + buffer.getOutputHeight());
            //System.out.println("x " + world.getCamera().getDirection().x);

            //buffer.blit(newYork, , 0, 0, 0, buffer.getOutputWidth(), buffer.getOutputHeight(), FrameBuffer.OPAQUE_BLITTING);
            buffer.setBlittingTarget(FrameBuffer.BLITTING_TARGET_FRONT);
            world.renderScene(buffer);
            world.draw(buffer);
            buffer.update();

            buffer.display(g, 0, 0);

        }

        public void run() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

//        public void mouseClicked(MouseEvent e) {
//            System.out.println("e View");
//            control.mouseClicked(e);
//        }
//
//        public void mousePressed(MouseEvent e) {
//            oldX = e.getX();
//            oldY = e.getY();
//        }
//
//        public void mouseReleased(MouseEvent e) {
//            throw new UnsupportedOperationException("Not supported yet.");
//        }
//
//        public void mouseEntered(MouseEvent e) {
//            throw new UnsupportedOperationException("Not supported yet.");
//        }
//
//        public void mouseExited(MouseEvent e) {
//            throw new UnsupportedOperationException("Not supported yet.");
//        }
        public void mouseDragged(MouseEvent e) {
            //  System.out.println("oldX " + oldX + " oldY " + oldY + " e.getX() " + e.getX() + " e.getY() " + e.getY() + " (oldX - e.getX())/1000 " + (oldX - e.getX())/1000f);
            System.out.println(world.getCamera().getXAxis().toString() + "  YA " + world.getCamera().getYAxis().toString());
            world.getCamera().rotateCameraY((oldX - e.getX()) / 100f);
            world.getCamera().rotateCameraX((oldY - e.getY()) / 100f);


            //world.getCamera().rotateCameraX((oldY - e.getY()) / 100f);

            //world.getCamera().rotateCameraX((e.getY() - oldY) / 100f);

            //world.getCamera().setEllipsoidMode(oldX)
            oldX = e.getX();
            oldY = e.getY();


        }
//        public void mouseMoved(MouseEvent e) {
////             world.getCamera().rotateCameraX((oldX - e.getX())/50);
////            world.getCamera().rotateCameraY((oldY - e.getY())/50);
////            oldX = e.getX();
////            oldY = e.getY();
//        }
//
//        public void mouseWheelMoved(MouseWheelEvent e) {
//            // System.out.println(" e.getWheelRotation() " + e.getWheelRotation());
//            world.getCamera().moveCamera(Camera.CAMERA_MOVEIN, e.getWheelRotation() * 25);
//        }
//
//        public void keyTyped(KeyEvent e) {
//            System.out.println("key typed=" + e.toString());
//        }
//
//        public void keyPressed(KeyEvent e) {
//            System.out.println("key pressed=" + e.toString());
//        }
//
//        public void keyReleased(KeyEvent e) {
//            System.out.println("key released=" + e.toString());
//        }
    }
}
