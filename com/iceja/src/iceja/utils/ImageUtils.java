/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package iceja.utils;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;

/**
 *
 * @author Catherine Ivanova
 * @date Jan 24, 2011
 */
public class ImageUtils {

    public static Image crop1(BufferedImage src, int x, int y, int w, int h) {
          return  src.getSubimage(x, y, w, h);

    }

    // fastest 100%
    public static Image scale1(Image src, int w, int h) {
        BufferedImage scaledImage = new BufferedImage(
                w, h, BufferedImage.TYPE_INT_ARGB);
        Graphics2D graphics2D = scaledImage.createGraphics();
        graphics2D.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
                RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        graphics2D.drawImage(src, 0, 0, w, h, null);

        graphics2D.dispose();
        return scaledImage;
    }

    // slower 120-230%
    public static Image scale2(Image src, int w, int h) {
        BufferedImage scaledImage = new BufferedImage(
                w, h, BufferedImage.TYPE_INT_ARGB);
        Graphics2D graphics2D = scaledImage.createGraphics();
        AffineTransform xform = AffineTransform.getScaleInstance(w, h);
        graphics2D.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
                RenderingHints.VALUE_INTERPOLATION_BICUBIC);
        graphics2D.drawImage(scaledImage, xform, null);
        graphics2D.dispose();
        return scaledImage;
    }

    // slowest 300-400%
    public static Image scale3(BufferedImage src, int w, int h) {
        BufferedImage scaledImage = new BufferedImage(
                w, h, BufferedImage.TYPE_INT_ARGB);

        AffineTransform scaleTransform = new AffineTransform();
// last-in-first-applied: rotate, scale
        scaleTransform.scale(w, h);
       // scaleTransform.rotate(Math.PI / 2, w / 2, h / 2);
        AffineTransformOp scaleOp = new AffineTransformOp(
                scaleTransform, AffineTransformOp.TYPE_BILINEAR);
        BufferedImage sc = scaleOp.filter(src, scaledImage);
        return scaledImage;
    }
}
