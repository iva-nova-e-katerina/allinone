/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package iceja;

import java.net.URL;

/**
 *
 * @author Catherine Ivanova
 * Jan 2, 2011
 */
public class Vertex {
    private URL url;


    public Vertex(URL url){
        this.url = url;
    }


    public URL getUrl() {
        return url;
    }


    public void setUrl(URL url) {
        this.url = url;
    }

}
