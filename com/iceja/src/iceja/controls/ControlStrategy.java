/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package iceja.controls;

import java.awt.event.KeyListener;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelListener;

/**
 *
 * @author Catherine Ivanova
 */
public interface ControlStrategy extends MouseListener, MouseMotionListener, MouseWheelListener, KeyListener{

  //  public void rotateXandY(ICJCamera camera, int xAngleLimit, int yAngleLimit, int xAngleDelta, int yAngleDelta, int xCurrentAngle, int yCurrentAngle);
    float FLIGHT_HEIGHT = 420f;
    float GRAVITY=4f;
    float COLLISION_SPHERE_RADIUS=8f;
    ICEJAVector ELLIPSOID_RADIUS=new ICEJAVector(COLLISION_SPHERE_RADIUS,FLIGHT_HEIGHT/2f,COLLISION_SPHERE_RADIUS);
    float MOVE_SPEED=2.5f;
    float TURN_SPEED=0.06f;
    int [] moveForward = {0,1};
    int [] moveBackward = {0,1};
}
