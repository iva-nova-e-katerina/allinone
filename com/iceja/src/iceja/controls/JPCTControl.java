/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package iceja.controls;


import com.threed.jpct.Camera;
import com.threed.jpct.FrameBuffer;
import com.threed.jpct.Interact2D;
import com.threed.jpct.Lights;
import com.threed.jpct.Matrix;
import com.threed.jpct.Object3D;
import com.threed.jpct.SimpleVector;
import com.threed.jpct.World;
import com.threed.jpct.util.KeyMapper;
import com.threed.jpct.util.KeyState;
import iceja.world.JPCTWorldBuilder;
import java.awt.Component;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.util.Timer;
import java.util.TimerTask;
import netscape.javascript.JSObject;

/**
 *
 * @author Catherine Ivanova
 * @date Feb 13, 2011
 */
public class JPCTControl extends AbstractControlStrategy {

    private World world = null;
    private FrameBuffer buffer = null;
    private ICJCamera camera;
    private Object3D plane;
    private Matrix playerDirection = new Matrix();
    private SimpleVector tempVector = new SimpleVector();
    private KeyMapper keyMapper = null;

    public JPCTControl(int startX, int startY, JSObject window, World world,
            JPCTWorldBuilder wb,
            FrameBuffer buffer, Object3D plane, Component panel) {
        super(wb, window, startX, startY);
        this.buffer = buffer;
        this.world = world;
        this.verticalEthalon = new ICEJAVector(world.getCamera().getXAxis().x,
                world.getCamera().getXAxis().y,
                world.getCamera().getXAxis().y);
        this.plane = plane;
        keyMapper = new KeyMapper(panel);
    }

    @Override
    public void mouseMoved(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {
        oldX = e.getX();
        oldY = e.getY();
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        //System.out.println(world.getCamera().getXAxis().toString() + "  YA " + world.getCamera().getYAxis().toString());
        //System.out.println(" new Y " + e.getY() + "  new x " + e.getX());
//            world.getCamera().rotateCameraY((oldX - e.getX()) / 100f);
//            world.getCamera().rotateCameraX((oldY - e.getY()) / 100f);


        //world.getCamera().rotateCameraX((oldY - e.getY()) / 100f);

        //world.getCamera().rotateCameraX((e.getY() - oldY) / 100f);

        //world.getCamera().setEllipsoidMode(oldX)

        if (invertMouse) {
            rotateXandY(world.getCamera(), 0, 0, oldX - e.getX(), oldY - e.getY(), 0, 0);
        } else {
            rotateXandY(world.getCamera(), 0, 0, oldX - e.getX(), oldY + e.getY(), 0, 0);
        }
        oldX = e.getX();
        oldY = e.getY();

    }

    @Override
    public void mouseClicked(MouseEvent e) {

        //            System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1 e.getY(), e.getX()"+  e.getY() +  " " +e.getX() );
//            SimpleVector dir = Interact2D.reproject2D3D(world.getCamera(), buffer, e.getX(), e.getY() );
//            dir.matMul(world.getCamera().getBack().invert3x3());
//            System.out.println("world.getVisibilityList() " + world.getVisibilityList().toString());
//            int[] result = Interact2D.pickPolygon(world.getVisibilityList(), dir);

        // play(getCodeBase(), "audio/gong.au");
        SimpleVector ray = Interact2D.reproject2D3DWS(world.getCamera(), buffer, e.getX(), e.getY());
        // SimpleVector ray = Interact2D.reproject2D3D(world.getCamera(), buffer, e.getX(), e.getY());
        //ray.matMul(world.getCamera().getBack().invert3x3());
        ///Object[] result = world.calcMinDistanceAndObject3D(world.getCamera().getPosition(), ray, 13000F);
        Object[] res = world.calcMinDistanceAndObject3D(world.getCamera().getPosition(), ray, 21000f /*or whatever*/);
        //System.out.println("e control" + res.length);
        if (res[1] != null) {
            //final Object3D picked = world.getObject(result[1]);
            final Object3D picked = (Object3D) res[1];
            //System.out.println("object found " + picked + " wb.blocks " + wb.blocks.size());
            //window.setMember("url", wb.blocks.get(picked).toString());
            boolean flag = (Boolean) window.call("test", new Object[]{wb.blocks.get(picked).toString()});
            //System.out.println("tested " + flag + " " + wb.blocks.get(picked).toString());
            Timer rotator = new Timer();
            rotator.scheduleAtFixedRate(new TimerTask() {

                @Override
                public void run() {

                    picked.rotateZ(-0.01f);
                }
            }, 1, 10);

        }
    }

    @Override
    public void mouseWheelMoved(MouseWheelEvent e) {
        world.getCamera().moveCamera(Camera.CAMERA_MOVEIN, e.getWheelRotation() * 25);
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        System.out.println("keyPressed" + e.getKeyCode());
        poll();
        doMovement();


        // 38 up
        //37 left
        // 40 down
        // 39 right
//        world.getCamera().moveCamera(Camera., oldX);
//        Camera cam = world.getCamera();
////		cam.moveCamera(Camera.CAMERA_MOVEOUT, 150);
////		cam.moveCamera(Camera.CAMERA_MOVEUP, 100);
//        cam.lookAt(plane.getTransformedCenter());
//        switch (e.getKeyCode()) {
//            case 37:
//                cam.rotateAxis(cam.getYAxis(), 0.04f);
//                break;
//            case 39:
//                cam.rotateAxis(cam.getYAxis(), -0.04f);
//                break;
//            case 38:
//                break;
//            case 40:
//                break;
//        }

    }

    @Override
    public void keyReleased(KeyEvent e) {
    }

    public void rotateXandY(Camera camera, int xAngleLimit, int yAngleLimit, int xAngleDelta, int yAngleDelta, int xCurrentAngle, int yCurrentAngle) {

        int dx = xAngleDelta;
        int dy = yAngleDelta;
        //System.out.println(camera.getXAxis().toString() + " " + camera.getYAxis().toString() + " dx:" + dx + " dy: " + dy);
        if (dx != 0) {
           // camera.rotateAxis(camera.getYAxis(), dx / 100f);
            camera.rotateAxis(camera.getBack().getYAxis(), dx / 100f);
            playerDirection.rotateY(dx / 100f);
        }

        if (dy != 0) {
            camera.rotateX(dy / 100f);
        }


        //float xAngle= 1.3962634f ; // some class attribute


//
//        int dx = xAngleDelta;
//        int dy = yAngleDelta;
//
//        if (dx!=0) {
//       	    camera.rotateAxis(camera.getYAxis(), dx / 100f);
//        }
//
//        if ((dy > 0 && xAngle < Math.PI / 4.2) || (dy < 0 && xAngle > -Math.PI / 4.2)) {
//                float t=dy/100f;
//        	camera.rotateX(t);
//        	xAngle += t;
//        }
    }

    private void keyAffected(KeyState state) {
        int code = state.getKeyCode();
        boolean event = state.getState();
        switch (code) {
            case (KeyEvent.VK_ESCAPE): {
                exit = event;
                break;
            }
            case (KeyEvent.VK_LEFT): {
                left = event;
                break;
            }
            case (KeyEvent.VK_RIGHT): {
                right = event;
                break;
            }
            case (KeyEvent.VK_PAGE_UP): {
                up = event;
                break;
            }
            case (KeyEvent.VK_PAGE_DOWN): {
                down = event;
                break;
            }
            case (KeyEvent.VK_UP): {
                forward = event;
                break;
            }
            case (KeyEvent.VK_DOWN): {
                back = event;
                break;
            }
            case (KeyEvent.VK_1): {
                if (event && buffer.supports(FrameBuffer.SUPPORT_FOR_RGB_SCALING)) {
                    world.getLights().setRGBScale(Lights.RGB_SCALE_DEFAULT);
                }
                break;
            }

            case (KeyEvent.VK_2): { // 2x scaling
                if (event && buffer.supports(FrameBuffer.SUPPORT_FOR_RGB_SCALING)) {
                    world.getLights().setRGBScale(Lights.RGB_SCALE_2X);
                }
                break;
            }

            case (KeyEvent.VK_4): { // 4x scaling
                if (event && buffer.supports(FrameBuffer.SUPPORT_FOR_RGB_SCALING)) {
                    world.getLights().setRGBScale(Lights.RGB_SCALE_4X);
                }
                break;
            }

//         case (KeyEvent.VK_W): { // wireframe mode (w)
//            if (event) {
//               wireframe=!wireframe;
//            }
//            break;
//         }
//
//         case (KeyEvent.VK_X): { // change renderer  (x)
//            if (event) {
//               switchMode=SWITCH_RENDERER;
//            }
//            break;
//         }
        }
    }

    public void doMovement() {
        /**
         * The first part handles the "physics", i.e. it allows the player to fall down.
         */
        Camera camera = world.getCamera();
        SimpleVector camPos = camera.getPosition();
        camPos.add(new SimpleVector(0, FLIGHT_HEIGHT / 2.0F, 0));
        SimpleVector dir = new SimpleVector(0, GRAVITY, 0);
        dir = world.checkCollisionEllipsoid(camPos, dir, ELLIPSOID_RADIUS.toSimpleVector(), 1);
        camPos.add(new SimpleVector(0, -FLIGHT_HEIGHT / 2.0F, 0));
        dir.x = 0;
        dir.z = 0;
        camPos.add(dir);
        camera.setPosition(camPos);
        /**
         * The "falling" part of the player is now finished. Now we care for the horizontal movement.
         * Nothing special here and no problems either. One thing worth mentioning is, that the player is
         * always moving in the same plane regardless of where he's looking. playerDirection takes
         * care of this.
         */
        // forward and back may change during excution (threaded!), so we have to ensure to
        // reset the camera only if has been changed before.
        boolean cameraChanged = false;
        if (forward) {
            camera.moveCamera(new SimpleVector(0, 1, 0), FLIGHT_HEIGHT / 2.0F);
            cameraChanged = true;
            tempVector = playerDirection.getZAxis();
            world.checkCameraCollisionEllipsoid(tempVector, ELLIPSOID_RADIUS.toSimpleVector(), MOVE_SPEED, 5);
        }
        if (back) {
            if (!cameraChanged) {
                camera.moveCamera(new SimpleVector(0, 1, 0), FLIGHT_HEIGHT / 2.0F);
                cameraChanged = true;
            }
            tempVector = playerDirection.getZAxis();
            tempVector.scalarMul(-1.0F);
            world.checkCameraCollisionEllipsoid(tempVector, ELLIPSOID_RADIUS.toSimpleVector(), MOVE_SPEED, 5);
        }
        if (left) {
            camera.rotateAxis(camera.getBack().getYAxis(), -TURN_SPEED);
            playerDirection.rotateY(-TURN_SPEED);
        }
        if (right) {
            camera.rotateAxis(camera.getBack().getYAxis(), TURN_SPEED);
            playerDirection.rotateY(TURN_SPEED);
        }
        if (up) {
            camera.rotateX(TURN_SPEED);
        }
        if (down) {
            camera.rotateX(-TURN_SPEED);
        }
        if (cameraChanged) {
            camera.moveCamera(new SimpleVector(0, -1, 0), FLIGHT_HEIGHT / 2.0F);
        }
    }

    public void poll() {
        KeyState state = null;
        do {
            state = keyMapper.poll();
            if (state != KeyState.NONE) {
                keyAffected(state);
            }
        } while (state != KeyState.NONE);
    }
    // public abstract void rotateXandY(ICJCamera camera, int xAngleLimit, int yAngleLimit, int xAngleDelta, int yAngleDelta, int xCurrentAngle, int yCurrentAngle) ;
}
