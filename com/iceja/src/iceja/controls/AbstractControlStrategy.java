/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package iceja.controls;

import iceja.world.JPCTWorldBuilder;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import netscape.javascript.JSObject;

/**
 *
 * @author Catherine Ivanova
 * @date Jan 23, 2011
 */
public abstract class AbstractControlStrategy implements ControlStrategy {

    int oldX;
    int oldY;
    byte movementSteps; // fixing direction and speed of movement when no forward or backward keys pressed
    JSObject window;
    JPCTWorldBuilder wb;
    boolean invertMouse = true;
    protected boolean left = false;
    protected boolean right = false;
    protected boolean up = false;
    protected boolean down = false;
    protected boolean forward = false;
    protected boolean back = false;
    protected boolean exit=false;
    ICEJAVector verticalEthalon;

    public abstract void mouseClicked(MouseEvent e);

    public void mousePressed(MouseEvent e) {
        oldX = e.getX();
        oldY = e.getY();
    }

    public AbstractControlStrategy(JPCTWorldBuilder wb, JSObject window, int oldX, int oldY) {
        this.oldX = oldX;
        this.oldY = oldY;
        this.window = window;
        this.wb = wb;

    }

    public abstract void mouseReleased(MouseEvent e);

    public abstract void mouseEntered(MouseEvent e);

    public abstract void mouseExited(MouseEvent e);

    public abstract void mouseDragged(MouseEvent e);

    public abstract void mouseMoved(MouseEvent e);

    public abstract void mouseWheelMoved(MouseWheelEvent e);

    public abstract void keyTyped(KeyEvent e);

    public abstract void keyPressed(KeyEvent e);
//    {
//        if(e.getKeyCode() == moveForward)
//    }

    public abstract void keyReleased(KeyEvent e);

  
    // public abstract void rotateXandY(ICJCamera camera, int xAngleLimit, int yAngleLimit, int xAngleDelta, int yAngleDelta, int xCurrentAngle, int yCurrentAngle) ;
}
