/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package iceja.controls;

import com.threed.jpct.SimpleVector;

/**
 *
 * @author Catherine Ivanova
 * @date Feb 20, 2011
 */
public class ICEJAVector {
    private float x;
    private float y;
    private float z;
    SimpleVector simpleVector;

    public ICEJAVector(float x, float y, float z){
        this.x = x;
        this.y = y;
        this.z = z;
        this.simpleVector = new SimpleVector(x, y, z);
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
        this.simpleVector.x =x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
        this.simpleVector.y =y;
    }

    public float getZ() {
        return z;
    }

    public void setZ(float z) {
        this.z = z;
        this.simpleVector.z = z;
    }

    public SimpleVector toSimpleVector(){

        return simpleVector;

    }

}
