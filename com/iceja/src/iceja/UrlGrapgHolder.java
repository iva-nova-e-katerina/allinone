/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package iceja;

import iceja.utils.ImageUtils;
import java.awt.EventQueue;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JFrame;
import org.jgrapht.DirectedGraph;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.DefaultEdge;
import org.lobobrowser.html.*;
import org.lobobrowser.html.gui.*;
import org.lobobrowser.html.parser.*;
import org.lobobrowser.html.test.SimpleHtmlRendererContext;
import org.lobobrowser.html.test.SimpleUserAgentContext;
import org.w3c.dom.Document;
import org.w3c.dom.html2.HTMLElement;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author Catherine Ivanova
 * Jan 4, 2011
 */
public class UrlGrapgHolder implements GraphBuilder {

    private static DirectedGraph<URL, DefaultEdge> hrefGraph;
    private static ConcurrentSkipListSet<HostURL> urls = new ConcurrentSkipListSet<HostURL>();

    /**
     * @return the hrefGraph
     */
    public static DirectedGraph<URL, DefaultEdge> getHrefGraph() {
        return hrefGraph;
    }

    /**
     * @return the urls
     */
    public static ConcurrentSkipListSet<HostURL> getUrls() {
        return urls;
    }

    /**
     * @param aUrls the urls to set
     */
    public static void setUrls(ConcurrentSkipListSet<HostURL> aUrls) {
        urls = aUrls;
    }
    //private static ArrayList<HostURL> urls = new ArrayList<HostURL>();
    private HostURL currentRoot;

    public UrlGrapgHolder(HostURL root) {
        //hrefGraph = createHrefGraph();
        currentRoot = root;
        // for very first element
        if (hrefGraph == null) {
            hrefGraph = new DefaultDirectedGraph<URL, DefaultEdge>(DefaultEdge.class);
//            hrefGraph.addVertex(root.getUrl());
//            urls.add(root);
        }

//        if (!hrefGraph.containsVertex(root.getUrl())) {
//            hrefGraph.addVertex(root.getUrl());
//            urls.add(root);
//        }
    }

    public void run() {
        try {
            Thread.sleep(5000);
            createGraph(currentRoot);
            //  System.out.println(urls.toString() + " depth " + currentRoot.getDepthFromRoot());
            //System.out.println(hrefGraph.toString());


        } catch (InterruptedException ex) {
            Logger.getLogger(UrlGrapgHolder.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(UrlGrapgHolder.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void connectToGraphRootAndChild(URL newUrl) {
        // hrefGraph.containsVertex(newUrl)
        synchronized (UrlGrapgHolder.class) {
            getHrefGraph().addVertex(newUrl);
            if (!getHrefGraph().containsVertex(currentRoot.getUrl())) {
                getHrefGraph().addVertex(currentRoot.getUrl());
            }
            if (!urls.contains(currentRoot)) {
                urls.add(currentRoot);
            }
            getHrefGraph().addEdge(currentRoot.getUrl(), newUrl);
        }
    }

//    public static synchronized DirectedGraph<URL, DefaultEdge> getHrefGraph() {
//        return hrefGraph;
//    }
    private DirectedGraph<URL, DefaultEdge> createHrefGraph() {
        DirectedGraph<URL, DefaultEdge> g =
                new DefaultDirectedGraph<URL, DefaultEdge>(DefaultEdge.class);

        //      try {
//            URL amazon = new URL("http://www.amazon.com");
//            URL yahoo = new URL("http://www.yahoo.com");
//            URL ebay = new URL("http://www.ebay.com");
//
//            // add the vertices
//            g.addVertex(amazon);
//            g.addVertex(yahoo);
//            g.addVertex(ebay);
//
//            // add edges to create linking structure
//            g.addEdge(yahoo, amazon);
//            g.addEdge(yahoo, ebay);
//        } catch (MalformedURLException e) {
//            e.printStackTrace();
//        }

        return g;
    }
    // private int depth = 0;
    private int depthLimit = 3;

    private void createGraph(HostURL root) throws IOException {
        currentRoot = root;
        if (root.getDepthFromRoot() >= depthLimit) {
            return;
        }

        // remote operations here
        ConcurrentSkipListSet<HostURL> reqUrls;

        String content = convertStreamToString(root.getUrl().openStream());
        //Image face = renderContent(content);

        String[] scanner = content.split("\\s*href=\\s*");
        for (String preUrl : scanner) {
            int begin = 0;
            char quotes = '\'';
            if (preUrl.startsWith("\"")) {
                quotes = '"';
                begin++;
            } else if (preUrl.startsWith("'")) {
                begin++;
            } else {
                quotes = ' ';

            }
            quotes = preUrl.indexOf(quotes, begin) <= -1 ? '>' : quotes;
            int end = preUrl.indexOf(quotes, begin);
            if (end <= -1 || end <= begin) {
                continue;
            }
            String urlString = preUrl.substring(begin, end);
            URL newUrl = null;
            try {
                newUrl = new URL(urlString);
            } catch (MalformedURLException ex) {
                // mailformed URL
                continue;
            }



            //  synchronization with client here

            HostURL toAdd = new HostURL(newUrl, currentRoot.getDepthFromRoot() + 1);
            //System.out.println(" contains "+ toAdd.getUrl().getHost() + "  ?? " + urls.contains(toAdd));
            if (!urls.contains(toAdd)) {
                //System.out.println("adding Host " + newUrl.getHost() + " depth " + toAdd.getDepthFromRoot() + " childof " + currentRoot.getUrl().getHost() + " Thread " + Thread.currentThread().getName() + " " + Thread.currentThread().getId());
                //connectToGraphRootAndChild(newUrl);

                Image rendered = null;
                try {

                    rendered = renderImage(content, toAdd);
                } catch (URISyntaxException ex) {
                    Logger.getLogger(UrlGrapgHolder.class.getName()).log(Level.SEVERE, null, ex);
                }
                //runNextNode(toAdd);


            } else {
                // System.out.println("already Added " + newUrl.getHost() + " Thread " + Thread.currentThread().getName() + " " + Thread.currentThread().getId());
            }

            // addUrl(urlString);
        }
        return;

    }

    private void runNextNode(HostURL toAdd) {
        Thread newUrlThread = new Thread(new UrlGrapgHolder(toAdd), toAdd.getUrl().getHost().toString());
        newUrlThread.start();
        //}
    }

    private static synchronized void addHostURL(HostURL url) {
        getUrls().add(url);
    }

    private Image renderImage(String content, final HostURL toAdd) throws URISyntaxException {
        try {
            // Initialize logging so only Cobra warnings are seen.
            // Open a connection on the URL we want to render first.
            //		String uri = "http://lobobrowser.org/browser/home.jsp";
            //		URL url = new URL(uri);
            //		URLConnection connection = url.openConnection();
            //		InputStream in = connection.getInputStream();
            // A Reader should be created with the correct charset,
            // which may be obtained from the Content-Type header
            // of an HTTP response.
            Reader reader = new StringReader(content);
            //Reader reader = new InputStreamReader(in);
            // InputSourceImpl constructor with URI recommended
            // so the renderer can resolve page component URLs.
            InputSource is = new InputSourceImpl(reader, toAdd.getUrl().toURI().toString());
            final GrabHtml htmlPanel = new GrabHtml();
            //    new SimpleHtmlRendererContext(panel, new SimpleUserAgentContext()).navigate("http://lobobrowser.org/browser/home.jsp");
            //   UserAgentContext ucontext = new LocalUserAgentContext();
            //   HtmlRendererContext rendererContext = new LocalHtmlRendererContext(htmlPanel, ucontext);

            UserAgentContext ucontext = new SimpleUserAgentContext();
            HtmlRendererContext rendererContext = new LocalHtmlRendererContext(htmlPanel, ucontext);
            // Set a preferred width for the HtmlPanel,
            // which will allow getPreferredSize() to
            // be calculated according to block content.
            // We do this here to illustrate the
            // feature, but is generally not
            // recommended for performance reasons.
            htmlPanel.setPreferredWidth(1024);
            // Note: This example does not perform incremental
            // rendering while loading the initial document.
            DocumentBuilderImpl builder = new DocumentBuilderImpl(rendererContext.getUserAgentContext(), rendererContext);
            Document document = builder.parse(is);
            reader.close();
            // Set the document in the HtmlPanel. This method
            // schedules the document to be rendered in the
            // GUI thread.
            htmlPanel.setDocument(document, rendererContext);
//            BufferedImage hello = new BufferedImage(htmlPanel.getWidth(), htmlPanel.getHeight(), BufferedImage.TYPE_INT_RGB);
            // htmlPanel.paint(htmlPanel.getGraphics());

            // Create a JFrame and add the HtmlPanel to it.
            final JFrame frame = new JFrame();
            frame.getContentPane().add(htmlPanel);
//            frame.pack();
//            frame.setVisible(true);

            System.out.println("width  " + htmlPanel.getWidth() + " height " + htmlPanel.getHeight() + "  " + htmlPanel.getBounds());
            //  System.exit(0);
//                    
            // We pack the JFrame to demonstrate the
            // validity of HtmlPanel's preferred size.
            // Normally you would want to set a specific
            // JFrame size instead.
            // pack() should be called in the GUI dispatch
            // thread since the document is scheduled to
            // be rendered in that thread, and is required
            // for the preferred size determination.
//
            Thread renderThread = new Thread(new Runnable() {

                public void run() {
                    getImage(frame, htmlPanel, toAdd);

                }
            });
            renderThread.setPriority(Thread.MIN_PRIORITY);
//
           EventQueue.invokeLater(renderThread);
            // getImage(frame, htmlPanel, toAdd);

//            try {
//                        ImageIO.write(htmlPanel.getImage(), "png", new File(Thread.currentThread().getName()));
//                    } catch (IOException ex) {
//                        Logger.getLogger(UrlGrapgHolder.class.getName()).log(Level.SEVERE, null, ex);
//                    }
//               final JFrame frame = new JFrame();
//       frame.getContentPane().add(htmlPanel);
//            frame.pack();
//            BufferedImage hello = new BufferedImage(htmlPanel.getWidth(), htmlPanel.getHeight(), BufferedImage.TYPE_INT_RGB);
//            Graphics g = hello.getGraphics();
//            htmlPanel.paint(g);
//            ImageIO.write(hello, "png", new File(Thread.currentThread().getName()));
        } catch (SAXException ex) {
            Logger.getLogger(UrlGrapgHolder.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(UrlGrapgHolder.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    private void getImage(JFrame frame, GrabHtml htmlPanel, HostURL toAdd) {
        frame.pack();
        // frame.setVisible(true);
        int width = htmlPanel.getWidth() > 2048 ? 2048 : htmlPanel.getWidth();
        int height = htmlPanel.getHeight() > 1024 ? 1024 : htmlPanel.getHeight();
        System.out.println("htmlPanel.getWidth()  " + htmlPanel.getWidth() + " htmlPanel.getHeight() " + htmlPanel.getHeight() + "  " + htmlPanel.getBounds());
        if (width > 0 && height > 0) {
            System.out.println("width  " + width + " height " + height + "  " + htmlPanel.getBounds());
            toAdd.setFace(htmlPanel.getImage());
            connectToGraphRootAndChild(toAdd.getUrl());
            getUrls().add(toAdd);
            //                        try {
            //                            //Thread.sleep(60000);
            //                        } catch (InterruptedException ex) {
            //                            Logger.getLogger(UrlGrapgHolder.class.getName()).log(Level.SEVERE, null, ex);
            //                        }
            //                         // runNextNode(toAdd);
            System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! " + Thread.currentThread().getPriority() + "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        }
    }

    public String convertStreamToString(InputStream is) throws IOException {
        if (is != null) {
            Writer writer = new StringWriter();
            char[] buffer = new char[1024];
            Reader reader = new BufferedReader(new InputStreamReader(is));
            int n;
            while ((n = reader.read(buffer)) != -1) {
                writer.write(buffer, 0, n);
            }
            writer.flush();
            return writer.toString();

        }

        return "";
    }
//    private void addUrl(String url) {
//        URL newUrl = null;
//        try {
//            newUrl = new URL(url);
//        } catch (MalformedURLException ex) {
//            // mailformed URL
//            return;
//        }
//        HostURL toAdd = new HostURL(newUrl);
//        if (!urls.contains(toAdd)) {
//            try {
//                System.out.println("adding Host " + newUrl.getHost() + " depth " + depth);
//
//
//                // hrefGraph.containsVertex(newUrl)
//                hrefGraph.addVertex(newUrl);
//                hrefGraph.addEdge(currentRoot, newUrl);
//                depth++;
//                Image rendered = createGraph(newUrl);
//                // if depth exceeded or something
//                if (rendered != null) {
//                    toAdd.setFace(rendered);
//                    urls.add(toAdd);
//                }
//            } catch (IOException ex) {
//                ex.printStackTrace();
//            } finally {
//                depth--;
//            }
//        } else {
//            System.out.println("already Added " + newUrl.getHost());
//        }
//    }

    /**
     * @return the urls
     */
//    public ConcurrentSkipListSet<HostURL> getUrls() {
//        return urls;
//    }
//
//    /**
//     * @param urls the urls to set
//     */
//    public void setUrls(ConcurrentSkipListSet<HostURL> urls) {
//        this.urls = urls;
//    }
    private static class LocalUserAgentContext
            extends SimpleUserAgentContext {
        // Override methods from SimpleUserAgentContext to
        // provide more accurate information about application.

        public LocalUserAgentContext() {
        }

        public String getAppMinorVersion() {
            return "0";
        }

        public String getAppName() {
            return "BarebonesTest";
        }

        public String getAppVersion() {
            return "1";
        }

        public String getUserAgent() {
            return "Mozilla/4.0 (compatible;) CobraTest/1.0";
        }
    }

    private static class LocalHtmlRendererContext
            extends SimpleHtmlRendererContext {
        // Override methods from SimpleHtmlRendererContext
        // to provide browser functionality to the renderer.

        public LocalHtmlRendererContext(HtmlPanel contextComponent,
                UserAgentContext ucontext) {
            super(contextComponent, ucontext);
        }

        public void linkClicked(HTMLElement linkNode,
                URL url, String target) {
            super.linkClicked(linkNode, url, target);
            // This may be removed:
            //System.out.println("## Link clicked: " + linkNode);
        }

        public HtmlRendererContext open(URL url,
                String windowName, String windowFeatures,
                boolean replace) {
            // This is called on window.open().
            HtmlPanel newPanel = new HtmlPanel();
            JFrame frame = new JFrame();
            frame.setSize(600, 400);
            frame.getContentPane().add(newPanel);
            HtmlRendererContext newCtx = new LocalHtmlRendererContext(newPanel, this.getUserAgentContext());
            newCtx.navigate(url, "_this");
            return newCtx;
        }
    }
}
