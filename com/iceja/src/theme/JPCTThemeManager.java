/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package theme;

import iceja.Main;
import iceja.utils.ImageUtils;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

/**
 *
 * @author Catherine Ivanova
 * @date Feb 26, 2011
 */
public class JPCTThemeManager extends AbstractThemeManager {

    public BufferedImage getForegraundTexture(String crudeFileName, int screenWidth, int screenHeight) {
        BufferedImage image = null;
        try {
            InputStream imageStream = new BufferedInputStream(Main.getTexture(crudeFileName));
            image = ImageIO.read(imageStream);
            // I should scale image to nearest 2 degree to jpct texture, then I need crop largeest dimension

            image.getWidth();
            int minXDegreeOf2 = (int) (Math.round(Math.log(screenWidth) / Math.log(2)));
            int minYDegreeOf2 = (int) (Math.round(Math.log(screenHeight) / Math.log(2)));
            int newHeight = 0, newWidth =0;

            // todo: change it to scale on width or height
            if (minXDegreeOf2 < minYDegreeOf2) {
                // scale width here
                newWidth = (int) Math.pow(2 , minXDegreeOf2);
                BufferedImage newImage = (BufferedImage) ImageUtils.scale1(image, newWidth, newWidth / screenWidth * screenHeight);
//                File img = new File("precrope.png");
//                ImageIO.write(newImage, "png", img);
//                System.out.println("saved " + img.getAbsolutePath());
                newHeight = (int) Math.pow(2, minYDegreeOf2);
                image = (BufferedImage) ImageUtils.crop1(image, 0, 0, newWidth, newHeight);
//                img = new File("postcrope.png");
//                ImageIO.write(image, "png", img);
//                System.out.println("saved " + img.getAbsolutePath());
            } else {
                // first degree of two in larger side
//                minXDegreeOf2 = (int) (Math.log(screenWidth) / Math.log(2)) + 1;
//                minYDegreeOf2 = (int) (Math.log(screenHeight) / Math.log(2)) + 1;
                  minXDegreeOf2 = (int) (Math.log(image.getWidth()) / Math.log(2)) + 1;
                  minYDegreeOf2 = (int) (Math.log(image.getHeight()) / Math.log(2)) + 1;

                //image = scaleHeightToDegreeOf2ThenCropToDegreeOf2(minYDegreeOf2, image, newHeight, minXDegreeOf2, newWidth);

                image = scaleWidthToDegreeOf2henCropToDegreeOf2(minXDegreeOf2, minYDegreeOf2, screenWidth, screenHeight, image);

                //image = scaleHeightToScreenHeightThenCropToDegreeOf2(screenWidth, screenHeight, image);
                //image = scaleWidthToScreenHeightThenCropToDegreeOf2(screenWidth, screenHeight, image);
            }

        } catch (IOException ex) {
            Logger.getLogger(JPCTThemeManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        return image;

    }

    private BufferedImage scaleHeightToDegreeOf2ThenCropToDegreeOf2(int minYDegreeOf2, BufferedImage image, int newHeight, int minXDegreeOf2, int newWidth) throws IOException {
      

        // scaling to 2 degree
        newHeight = (int) Math.pow(2 , minYDegreeOf2);
        BufferedImage newImage = (BufferedImage) ImageUtils.scale1(image, (int)((float)newHeight / (float)image.getHeight() * image.getWidth()), newHeight);
                        File img = new File("precrope.png");
                        ImageIO.write(newImage, "png", img);
                        System.out.println("saved " + img.getAbsolutePath());
        newWidth = (int) Math.pow(2 , minXDegreeOf2);
        image = (BufferedImage) ImageUtils.crop1(newImage, 0, 0, newWidth, newHeight);
                        img = new File("postcrope.png");
                        ImageIO.write(image, "png", img);
                        System.out.println("saved " + img.getAbsolutePath());
        return image;
    }

    private BufferedImage scaleWidthToScreenHeightThenCropToDegreeOf2(int screenWidth, int screenHeight,  BufferedImage image) throws IOException {

            int minXDegreeOf2 = (int) (Math.log(screenWidth) / Math.log(2)) ;
            int minYDegreeOf2 = (int) (Math.log(screenHeight) / Math.log(2));

        int newHeight = (int) Math.pow(2 , minYDegreeOf2);

        BufferedImage newImage = (BufferedImage) ImageUtils.scale1(image, screenWidth, (int)((float)image.getWidth() / screenWidth * image.getHeight()));
                        File img = new File("precrope.png");
                        ImageIO.write(newImage, "png", img);
                        System.out.println("saved " + img.getAbsolutePath());
        int newWidth = (int) Math.pow(2 , minXDegreeOf2);
        image = (BufferedImage) ImageUtils.crop1(newImage, 0, 0, newWidth, newHeight);
                        img = new File("postcrope.png");
                        ImageIO.write(image, "png", img);
                        System.out.println("saved " + img.getAbsolutePath());
        return image;
    }


     private BufferedImage scaleWidthToDegreeOf2henCropToDegreeOf2(int minXDegreeOf2, int minYDegreeOf2, int screenWidth, int screenHeight,  BufferedImage image) throws IOException {

        int newWidth = (int) Math.pow(2 , minXDegreeOf2);
        BufferedImage newImage = (BufferedImage) ImageUtils.scale1(image, newWidth, (int)((float)image.getWidth() / (float)newWidth * image.getHeight()));
        File img = new File("precrope.png");
        ImageIO.write(newImage, "png", img);
        System.out.println("saved " + img.getAbsolutePath());
        int newHeight = (int) Math.pow(2 , minYDegreeOf2);
        newHeight = newHeight > newImage.getHeight() ? (int) Math.pow(2 , minYDegreeOf2 - 1)  : newHeight;
        image = (BufferedImage) ImageUtils.crop1(newImage, 0, 0, newWidth, newHeight);
        img = new File("postcrope.png");
        ImageIO.write(image, "png", img);
        System.out.println("saved " + img.getAbsolutePath());
        return image;
    }



     private BufferedImage scaleHeightToScreenHeightThenCropToDegreeOf2(int screenWidth, int screenHeight,  BufferedImage image) throws IOException {

            int minXDegreeOf2 = (int) (Math.log(screenWidth) / Math.log(2));
            int minYDegreeOf2 = (int) (Math.log(screenHeight) / Math.log(2));

        int newHeight = (int) Math.pow(2 , minYDegreeOf2);

        BufferedImage newImage = (BufferedImage) ImageUtils.scale1(image, (int)((float)screenWidth / (float)screenHeight * screenWidth), screenHeight);
                        File img = new File("precrope.png");
                        ImageIO.write(newImage, "png", img);
                        System.out.println("saved " + img.getAbsolutePath());
        int newWidth = (int) Math.pow(2 , minXDegreeOf2);
        image = (BufferedImage) ImageUtils.crop1(newImage, 0, 0, newWidth, newHeight);
                        img = new File("postcrope.png");
                        ImageIO.write(image, "png", img);
                        System.out.println("saved " + img.getAbsolutePath());
        return image;
    }
    public InputStream getISfromImage(BufferedImage bufimage){
         ByteArrayOutputStream os = new ByteArrayOutputStream();
        try {
   
            ImageIO.write(bufimage, "jpg", os);
            
        } catch (IOException ex) {
            Logger.getLogger(JPCTThemeManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        return new ByteArrayInputStream(os.toByteArray());
    }

}
