/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package theme;

import java.awt.image.BufferedImage;
import java.io.InputStream;

/**
 *
 * @author Catherine Ivanova
 * @date Feb 26, 2011
 */
public interface ThemeManager {
    BufferedImage getForegraundTexture(String crudeFileName, int screenWith, int screenHeight);
    InputStream getISfromImage(BufferedImage bufimage);

}
