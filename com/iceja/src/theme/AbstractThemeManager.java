/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package theme;

import java.awt.image.BufferedImage;

/**
 *
 * @author Catherine Ivanova
 * @date Feb 26, 2011
 */
public abstract class AbstractThemeManager implements ThemeManager{

    public abstract BufferedImage getForegraundTexture(String crudeFileName, int screenWith, int screenHeight);

}
