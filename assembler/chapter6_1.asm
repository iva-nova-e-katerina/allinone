        segment .data
x1      dq      0
y1      dq      0
x2      dq      10
y2      dq      10
katet1  dq      0
katet2  dq      0
hipo    dq      0


        segment .text
        global  _start
_start:
        mov     rax ,[x2]
        mov     rbx ,[x1]
        sub     rax ,rbx  
        mov     rbx ,rax       ; getting absolute value
        neg     rax
        cmovl   rax , rbx
        mov     [katet1], rax
        mov     rcx, rax
        mov     rax ,[y2]
        mov     rbx ,[y1]
        sub     rax ,rbx  
        mov     rbx ,rax       ; getting absolute value
        neg     rax
        cmovl   rax , rbx
        mov     [katet2], rax
        imul    rcx, [katet1]
        imul    rax, [katet2]
        add     rcx, rax     
        mov     [hipo], rcx    
        cvtsi2ss xmm0, rcx
        sqrtss   xmm0, xmm0
        cvtss2si rcx, xmm0     

        ret
