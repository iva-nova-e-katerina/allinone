        segment .data
a       db      1
aa       db      -1
b       dw      -0x2
c       dd      -1
d       dq      -7

        section .text
        global  _start        ; let the linker know about main
_start:
        movsx rax , byte [a]
        movsx rax , byte [aa]        
        movsx rbx , word [b]
        movsxd rcx , dword [c]
        mov rdx, qword [d]
        add rax , rbx
        add rax , rcx
        add rax , rdx
        mov [a]  , rax
        ret
