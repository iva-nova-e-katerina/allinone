        segment .data
x1      dq      100
y1      dq      200
x2      dq      100
y2      dq      30
katet1  dq      1
katet2  dq      0


        segment .text
        global  _start
_start:
        mov     rax ,[x2]
        mov     rbx ,[x1]
        sub     rax ,rbx  
        mov     rcx ,rax  
        mov     rdx ,rax          
        cmovnz  rcx ,[katet2]
        cmovz   rdx ,[katet1]
        cmovz   rax ,rdx
        cmovnz  rax ,rcx        
        ret
