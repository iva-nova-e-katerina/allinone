        segment .data
a       db      1
aa      db      -1

        section .text
        global  _start        ; let the linker know about main
_start:
        movsx rax , byte [a]
        movsx rax , byte [aa]        
        movzx rax , byte [a]
        movzx rax , byte [aa]              
        ret
