        segment .data
x1      dq      100
y1      dq      200
x2      dq      0
y2      dq      30
katet1  dq      1
katet2  dq      0


        segment .text
        global  _start
_start:
        mov     rax ,[x2]
        mov     rbx ,[x1]
        sub     rax ,rbx  
        cmovnz  rax ,[katet1]
        mov     rcx ,rax  
        cmovz   rcx ,[katet1]
        cmovz   rax ,rcx
        ret
