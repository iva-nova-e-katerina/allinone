        segment .data
costperkilowatthour       dq      73.5
ammountdollars      dq      0
ammountcents      dq      0
kilowatts   dq  9199
over1000 dq  0
tmp dq  100.0

        segment .text
        global  _start
_start:
        xor rax, rax;
        xor rbx, rbx;        
        mov rax, qword 1000;
        sub rax, qword [kilowatts]; 
        cmovl rbx, rax
        mov rax, rbx
        neg     rbx
        cmovl   rbx , rax                                                                      
        mov [over1000], rbx 
        mov rax, [over1000]
        cvtsi2sd xmm0, rax
        movupd xmm1, [costperkilowatthour]
        mulsd   xmm0, xmm1
        cvtsd2si rax, xmm0     
        mov [ammountdollars], rax;        
        cvtsi2sd xmm1, rax
        subsd  xmm0, xmm1
        mulsd  xmm0, [tmp]
        cvtsd2si rax, xmm0     
        mov [ammountcents], rax;             
        ret

