        segment .data
a      dq      00010101001101000000010101110101b
b      dq      01010101001101001110010101110101b
#                1010101001101001110010101110101
#                 10101001101000000010101110101


        segment .text
        global  _start
_start:
        mov     rbx, qword [b] 
        xor     qword [a], rbx
        mov     rcx, qword [a]
        xor     rcx, rbx
        mov     qword [b], rcx
        xor     qword [a], rcx
        ret
        