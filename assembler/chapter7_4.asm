        segment .data
sign        dq      0
fraction    dq      0
exponent    dq      0
input       dq      -13.123
bias        dq      1111111111b

        segment .text
        global  _start
        
_start:
        xor     rax, rax
        bt      qword [input], 64
        setc    al
        mov     [sign], al
        mov     rax, [input]
        shl     rax, 1
        shr     rax, 53
        sub     rax, [bias]
        mov     [exponent], rax
        mov     rax,  [input]        
        shl     rax, 12
        shr     rax, 12
        mov     [fraction], rax
        leave
        ret
        