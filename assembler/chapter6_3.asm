        segment .data
x1      dq      4.4
x2      dq      76.9
x3      dq      12.11
x4      dq      10.666
x5      dq      4
x6      dq      76
x7      dq      12
x8      dq      10
num     dq      4
awerage dq      0
reminder dq     0
sum     dq      0

        segment .text
        global  _start
_start:
        xor     rbx, rbx    ;FLOAT (doesnt work)
        movsd   xmm0 , qword [x1]
        addsd   xmm0 , qword [x2]            
        addsd   xmm0 , qword [x3]            
        addsd   xmm0 , qword [x4]                    
        movsd   [sum], xmm0
        movsd   xmm1, qword [num]                    
        divpd   xmm0, xmm1;

        
        xor     rbx, rbx          ; INTEGER (work fine)
        add     rbx,  qword [x5]
        add     rbx,  qword [x6]
        add     rbx,  qword [x7]
        add     rbx,  qword [x8]                        
        mov     [sum], qword rbx
        xor     rax , rax    ; put 0 in rax
        mov     rax , rbx     ;
        mov     rbx , qword [num]
        div     rbx         ;divide by num
        mov     [awerage] , rax ;store the quotient
        mov     [reminder], rdx ;store the remaind  
        ret

