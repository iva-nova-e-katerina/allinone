        segment .data
a       dq      1
b       dq      0x2
c       dq      -1
d       dq      -2

        segment .bss
k       times    5 resb 5 

        section .text
        global  main        ; let the linker know about main
main:
        mov rax , [a]
        mov rbx , [b]
        mov rcx , [c]
        mov rdx , [d]
        mov r8  , rax
        add r8  , rbx
        add r8  , rcx        
        add r8  , rdx                
        ret
